# wallet-rpc

#### 项目介绍
支持各个币种的RPC,RPC的部署及测试都需要有相关钱包节点的支撑
(缺失jar包在jar文件夹中)

本项目借鉴了ztuo开源项目并使用了一些代码。



## BTC 

bitcoin.conf

```
datadir=/data/nodes/bitcoin
dbcache=10240
txindex=1
rpcuser=bitcoin
rpcpassword=bitcoin
daemon=1
server=1
rest=1
rpcbind=0.0.0.0:8332
rpcallowip=0.0.0.0/0
deprecatedrpc=accounts
addnode=119.23.67.156
addnode=47.224.175.1
addnode=39.105.39.182
addnode=120.24.70.214
addnode=39.100.228.213
addnode=43.226.37.242
addnode=121.18.238.39
addnode=42.59.56.174
```


./bin/bitcoind  --conf=/root/bitcoin-0.20.0/bitcoin.conf



https://mistydew.github.io/blog/2018/05/bitcoin-cli-commands.html

./bin/bitcoin-cli  -rpcuser=bitcoin -rpcpassword=bitcoin help

./bin/bitcoin-cli  -rpcuser=bitcoin -rpcpassword=bitcoin getnewaddress huzhenyuan legacy



./bin/bitcoin-cli  -rpcuser=bitcoin -rpcpassword=bitcoin dumpprivkey  bc1qd77uwkfcgev4lar6uy5lk3s4shh00ydfxtndkd



sudo python3 wt_extract_keys.py -d ~/nodes/bitcoin/wallet.dat -v 0 > adds.txt